import { Provider } from 'react-redux';
import store from '../../src/store';
import Picture from '../../src/components/Avatar/Picture';
import Tools from '../../src/components/Avatar/Tools';
import Table from '../../src/components/UI/Table/Table';
import Footer, { HelpLink } from '../../src/components/UI/Footer/Footer';
import {
  bgBlack,
  bgWhite,
  bgFire,
  body1,
  eyes1,
  eyes2,
  hair1,
  hair2,
  hat1,
  hat2,
  mouth1
} from '../../src/components/Avatar/images';

const parts = [
  { value: 0, label: 'background' },
  { value: 1, label: 'hats' },
  { value: 2, label: 'hair' },
  { value: 3, label: 'eyes' },
  { value: 4, label: 'mouth' },
  { value: 5, label: 'body' }
];

const images = [
  [bgWhite, bgBlack, bgFire],
  [hat1, hat2],
  [hair1, hair2],
  [eyes1, eyes2],
  [mouth1],
  [body1],
];

const AvatarPage = () => {
  const content = [];
  let num = 0;
  images.forEach((i) => {
    const array = [];
    i.forEach((im) => {
      array.push({ id: num, image: im });
      num += 1;
    });
    content.push(array);
  });

  return (
    <Provider store={store}>
      <Table>
        <Picture />
        <Tools parts={parts} images={content} />
      </Table>
      <Footer>
        <HelpLink path="/avatar/help" label="Help" />
      </Footer>
    </Provider>
  );
};

export default AvatarPage;
