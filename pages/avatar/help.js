import Link from 'next/link';
import Footer from '../../src/components/UI/Footer/Footer';

const Help = () => {
  return (
    <>
      <Link href="/avatar">← back</Link>
      <div className="text-center">
        <h2>Create your own avatar</h2>
        <p>Here You can construct funny profile picture using provided cartoony face features</p>
        <p>Create some goofy character and give it a name of Your choice</p>
      </div>
      <Footer />
    </>
  );
};

export default Help;
