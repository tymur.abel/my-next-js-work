import PropTypes from 'prop-types';
import { fullURL } from './api/weather';
import Weather from '../src/components/Weather/Weather';
import Footer from '../src/components/UI/Footer/Footer';

const WeatherPage = ({ weatherData }) => {
  return (
    <>
      <Weather data={weatherData} />
      <Footer />
    </>
  );
};

export const getStaticProps = async () => {
  const response = await fetch(fullURL);
  const data = await response.json();
  return {
    props: { weatherData: data.daily },
    revalidate: 7200
  };
};

WeatherPage.propTypes = {
  weatherData: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default WeatherPage;
