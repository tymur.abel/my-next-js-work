const base = 'https://api.openweathermap.org/data/2.5/onecall?';
const lat = '48.50925841294777';
const lon = '32.26588538641969';
const appid = 'ff04fc01f3c79f92f523244010e88079';
const exl = 'current,minutely,hourly,alerts';
const fullURL = `${base}lat=${lat}&lon=${lon}&exlude=${exl}&units=metric&appid=${appid}`;

const handler = async (req, res) => {
  const request = await fetch(fullURL);
  const data = await request.json();
  res.status(200).json(data);
};

export { fullURL };

export default handler;
