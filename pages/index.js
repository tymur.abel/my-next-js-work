import { Provider } from 'react-redux';
import store from '../src/store';
import App from '../src/components/App/App';

const HomePage = () => {
  return <Provider store={store}><App /></Provider>;
};

export default HomePage;
