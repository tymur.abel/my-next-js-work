import PropTypes from 'prop-types';
import { useState, useRef } from 'react';
import classes from './Control.module.css';

const Control = ({ onAddItem }) => {
  const [controlsVisible, setVisible] = useState(false);
  const [nameInputValid, setNameInputValid] = useState(true);
  const itemNameRef = useRef();

  const showControlsHandler = () => { setVisible(true); };
  const hideControlsHandler = (event) => {
    event.preventDefault();
    setVisible(false);
  };
  const Validate = (str0) => {
    if (str0 === '') {
      setNameInputValid(false);
      return null;
    }
    setNameInputValid(true);
    return str0;
  };
  const submitFormHandler = (event) => {
    event.preventDefault();
    const value = Validate(itemNameRef.current.value);
    if (value !== null) {
      onAddItem({
        id: Math.random().toString(),
        name: value,
        checked: false,
      });
      setVisible(false);
    }
  };

  return (
    <div className={controlsVisible ? classes['list-card'] : classes['center-objects']}>
      {!controlsVisible && <button type="button" onClick={showControlsHandler}>Add Item</button>}
      {controlsVisible && (
      <form onSubmit={submitFormHandler}>
        <div>
          <p className={classes['half-width']}>Name</p>
          <input
            ref={itemNameRef}
            type="text"
            className={nameInputValid ? classes['half-width'] : `${classes['half-width']} ${classes.invalid}`}
          />
        </div>
        <button type="submit">Add</button>
        <button type="button" onClick={hideControlsHandler}>Cancel</button>
      </form>
      )}
    </div>
  );
};

Control.propTypes = {
  onAddItem: PropTypes.func.isRequired
};

export default Control;
