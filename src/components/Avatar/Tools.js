import { useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Image from 'next/image';
import { avatarActions } from '../../store';
import Tab from '../UI/Tabs/Tabs';
import classes from './Layout.module.css';

const bgLayer = 0;
const hatLayer = 1;
const hairLayer = 2;
const eyeLayer = 3;
const mouthLayer = 4;
const bodyLayer = 5;

const Tools = ({ parts, images }) => {
  const [select, setSelect] = useState(0);
  const dispatch = useDispatch();
  const tabClickHandler = (id) => {
    setSelect(id);
  };
  const onChangeLayer = (layer, value) => {
    switch (layer) {
      case bgLayer:
        dispatch(avatarActions.setBackground(value.image));
        break;
      case bodyLayer:
        dispatch(avatarActions.setBody(value.image));
        break;
      case hairLayer:
        dispatch(avatarActions.setHair(value.image));
        break;
      case hatLayer:
        dispatch(avatarActions.setHat(value.image));
        break;
      case mouthLayer:
        dispatch(avatarActions.setMouth(value.image));
        break;
      case eyeLayer:
        dispatch(avatarActions.setEyes(value.image));
        break;
      default: throw new Error();
    }
  };

  return (
    <div className={classes.tools}>
      <Tab options={parts} select={select} onTabClick={tabClickHandler} />
      <div className={classes.folder}>
        {images[select].map((im) => {
          return (
            <button type="button" key={im.id} className={classes.images} onClick={() => onChangeLayer(select, im)}>
              <Image
                src={im.image}
                alt={parts[select].label}
                layout="fill"
              />
            </button>
          );
        })}
      </div>
    </div>
  );
};

Tools.propTypes = {
  parts: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired
  })).isRequired,
  images: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({})).isRequired).isRequired
};

export default Tools;
