import { useSelector } from 'react-redux';
import Image from 'next/image';
import { portrait } from './images';
import classes from './Layout.module.css';

const Picture = () => {
  const layers = useSelector((state) => state.avatarManager.layers);

  return (
    <div className={classes.portrait}>
      {layers.background && <Image src={layers.background} alt="my avatar" layout="fill" />}
      <Image src={portrait} alt="my avatar" layout="fill" priority />
      {layers.body && <Image src={layers.body} alt="my avatar" layout="fill" />}
      {layers.mouth && <Image src={layers.mouth} alt="my avatar" layout="fill" />}
      {layers.eyes && <Image src={layers.eyes} alt="my avatar" layout="fill" />}
      {layers.hair && <Image src={layers.hair} alt="my avatar" layout="fill" />}
      {layers.hat && <Image src={layers.hat} alt="my avatar" layout="fill" />}
      <input type="text" className={classes.nameInput} />
    </div>
  );
};

export default Picture;
