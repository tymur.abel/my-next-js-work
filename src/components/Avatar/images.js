import bgBlack from '../../images/bg-black.png';
import bgWhite from '../../images/bg-white.png';
import bgFire from '../../images/bg-fire.png';
import body1 from '../../images/body_1.png';
import eyes1 from '../../images/eyes_1.png';
import eyes2 from '../../images/eyes_2.png';
import hair1 from '../../images/hair_1.png';
import hair2 from '../../images/hair_2.png';
import hat1 from '../../images/hat_1.png';
import hat2 from '../../images/hat_2.png';
import mouth1 from '../../images/mouth_1.png';
import portrait from '../../images/portrait.png';

export {
  bgBlack,
  bgWhite,
  bgFire,
  body1,
  eyes1,
  eyes2,
  hair1,
  hair2,
  hat1,
  hat2,
  mouth1,
  portrait
};
