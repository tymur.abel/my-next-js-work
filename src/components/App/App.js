import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { itemManagerActions } from '../../store';
import List from '../List/List';
import Control from '../Control/Control';
import Footer from '../UI/Footer/Footer';

const App = () => {
  const items = useSelector((state) => state.itemManager.items);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedList = localStorage.getItem('my_list');
    if (storedList !== null) {
      const list = JSON.parse(storedList);
      dispatch(itemManagerActions.setItems(list));
    }
  }, [dispatch]);

  const addItemHandler = (item) => {
    dispatch(itemManagerActions.addItem(item));
  };
  const removeItemHandler = (name) => {
    dispatch(itemManagerActions.removeItem(name));
  };

  return (
    <>
      <List items={items} onRemoveItem={removeItemHandler} onAddItem={addItemHandler} />
      <br />
      <Control onAddItem={addItemHandler} />
      <Footer />
    </>
  );
};

export default App;
