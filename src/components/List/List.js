import PropTypes from 'prop-types';
import ListItem from './ListItem';
import classes from './List.module.css';

const List = ({ items, onRemoveItem, onAddItem }) => {
  let listDom = <p className={classes['text-center']}>This list is empty</p>;

  if (items && items.length > 0) {
    listDom = (
      <ul className={classes['list-card']}>
        {items.map((item) => (
          <ListItem
            key={item.id}
            name={item.name}
            checked={item.checked}
            onRemoveItem={onRemoveItem}
            onAddItem={onAddItem}
          />
        ))}
      </ul>
    );
  }

  return listDom;
};

List.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onRemoveItem: PropTypes.func.isRequired,
  onAddItem: PropTypes.func.isRequired
};

export default List;
