import PropTypes from 'prop-types';
import classes from './List.module.css';

const ListItem = ({
  name, checked, onRemoveItem, onAddItem
}) => {
  const checkboxHandler = () => {
    onAddItem({ name, checked: !checked });
  };

  const removeItemHandler = () => {
    onRemoveItem(name);
  };

  return (
    <li>
      <input className={classes.check} type="checkbox" checked={checked} onChange={checkboxHandler} />
      <h2 className={classes.inline}>{name}</h2>
      <button type="button" onClick={removeItemHandler}>X</button>
    </li>
  );
};

ListItem.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onAddItem: PropTypes.func.isRequired,
  onRemoveItem: PropTypes.func.isRequired
};

export default ListItem;
