import Link from 'next/link';
import PropTypes from 'prop-types';
import classes from './Footer.module.css';

const HelpLink = ({ path, label }) => {
  return (
    <footer className={classes.footer}>
      <Link className={classes.link} href={path}>{label}</Link>
    </footer>
  );
};

HelpLink.propTypes = {
  path: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

export default HelpLink;
