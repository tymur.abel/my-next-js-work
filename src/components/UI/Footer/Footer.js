import PropTypes from 'prop-types';
import Link from 'next/link';
import classes from './Footer.module.css';

const HelpLink = ({ path, label }) => {
  return <Link className={classes.link} href={path}>{label}</Link>;
};

const Footer = ({ children }) => {
  const extras = children !== undefined;

  return (
    <footer className={classes.footer}>
      {extras && children}
      <a>YouTube</a>
      <a>Facebook</a>
      <a>Instagram</a>
    </footer>
  );
};

HelpLink.propTypes = {
  path: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
};

Footer.defaultProps = {
  children: undefined
};

Footer.propTypes = {
  children: PropTypes.shape({})
};

export { HelpLink };

export default Footer;
