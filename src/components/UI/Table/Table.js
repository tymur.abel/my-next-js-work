import PropTypes from 'prop-types';
import classes from './Table.module.css';

const Table = ({ children }) => {
  return (
    <table className={classes.myTable}>
      <tbody>
        <tr>
          <td className={classes.cell}>{children[0]}</td>
          <td className={classes.cell}>{children[1]}</td>
        </tr>
      </tbody>
    </table>
  );
};

Table.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired
};

export default Table;
