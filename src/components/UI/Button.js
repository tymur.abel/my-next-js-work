import PropTypes from 'prop-types';

const Button = ({
  id, label, className, onButtonClick
}) => {
  const onClickHandler = () => {
    onButtonClick(id);
  };

  return (
    <button
      type="button"
      className={className}
      onClick={onClickHandler}
    >
      {label}

    </button>
  );
};

Button.propTypes = {
  id: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  onButtonClick: PropTypes.func.isRequired
};

export default Button;
