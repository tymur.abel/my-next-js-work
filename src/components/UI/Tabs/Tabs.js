import PropTypes from 'prop-types';
import Button from '../Button';
import classes from './Tabs.module.css';

const Tab = ({ options, select, onTabClick }) => {
  return (
    <ul className={classes.container}>
      {options.map((o) => {
        return (
          <li key={o.value}>
            <Button
              label={o.label}
              id={o.value}
              onButtonClick={onTabClick}
              className={select === options.indexOf(o) ? classes.selected : classes.normal}
            />
          </li>
        );
      })}
    </ul>
  );
};

Tab.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
    label: PropTypes.string
  })).isRequired,
  select: PropTypes.number.isRequired,
  onTabClick: PropTypes.func.isRequired
};

export default Tab;
