import PropTypes from 'prop-types';
import classes from './Weather.module.css';

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const Weather = ({ data }) => {
  if (!data.length) return <h1>The weather is not availible</h1>;
  return (
    <ul className={classes.panel}>
      {data.map((day) => {
        return (
          <li key={day.dt} className={classes.item}>
            <p className={classes.day}>{days[new Date(+`${day.dt}000`).getDay()]}</p>
            <p className={classes.temp}>{`${day.temp.day} °C`}</p>
            <p className={classes.weather}>{day.weather[0].main}</p>
          </li>
        );
      })}
    </ul>
  );
};

Weather.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default Weather;
