import { useRouter } from 'next/router';
import Link from 'next/link';
import classes from './Header.module.css';

const Header = () => {
  const router = useRouter();
  const path = router.route;

  return (
    <div className={classes.header}>
      <Link href="/weather">
        <a className={`${classes.myLink} ${path === '/weather' ? classes.active : ''}`}>Weather</a>
      </Link>
      <Link href="/">
        <a className={`${classes.myLink} ${path === '/' ? classes.active : ''}`}>My List</a>
      </Link>
      <Link href="/avatar">
        <a className={`${classes.myLink} ${path === '/avatar' ? classes.active : ''}`}>Avatar</a>
      </Link>
    </div>
  );
};

export default Header;
