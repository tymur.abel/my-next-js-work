import { configureStore, createSlice, current } from '@reduxjs/toolkit';

const avatarManager = createSlice({
  name: 'myAvatar',
  initialState: { layers: {} },
  reducers: {
    setLayers(state, action) {
      state.layers = action.payload;
    },
    setBackground(state, action) {
      state.layers.background = (current(state).layers.background !== action.payload)
        ? action.payload
        : null;
    },
    setHat(state, action) {
      state.layers.hat = (current(state).layers.hat !== action.payload)
        ? action.payload
        : null;
    },
    setHair(state, action) {
      state.layers.hair = (current(state).layers.hair !== action.payload)
        ? action.payload
        : null;
    },
    setEyes(state, action) {
      state.layers.eyes = (current(state).layers.eyes !== action.payload)
        ? action.payload
        : null;
    },
    setMouth(state, action) {
      state.layers.mouth = (current(state).layers.mouth !== action.payload)
        ? action.payload
        : null;
    },
    setBody(state, action) {
      state.layers.body = (current(state).layers.body !== action.payload)
        ? action.payload
        : null;
    },
  }
});

const manageListItems = createSlice({
  name: 'itemManager',
  initialState: { items: [] },
  reducers: {
    setItems(state, action) {
      state.items.splice(0);
      action.payload.forEach((e) => state.items.push(e));
    },
    addItem(state, action) {
      const item = action.payload;
      if (item == null) return;
      const existingItem = state.items.find((elem) => elem.name === item.name);
      if (!existingItem) state.items.push(item);
      else existingItem.checked = item.checked;
      localStorage.setItem('my_list', JSON.stringify(state.items));
    },
    removeItem(state, action) {
      const newItems = state.items.filter((item) => item.name !== action.payload);
      state.items.splice(0);
      newItems.forEach((e) => state.items.push(e));
      localStorage.setItem('my_list', JSON.stringify(newItems));
    }
  }
});

const store = configureStore({
  reducer: {
    itemManager: manageListItems.reducer,
    avatarManager: avatarManager.reducer
  }
});

export const itemManagerActions = manageListItems.actions;

export const avatarActions = avatarManager.actions;

export default store;
